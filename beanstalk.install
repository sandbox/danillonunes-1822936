<?php

/**
 * @file
 * Install, update and uninstall functions for the beanstalk module.
 */

/**
 * Implementation of hook_install().
 */
function beanstalk_install() {
  drupal_install_schema('beanstalk');
}

/**
 * Implementation of hook_uninstall().
 */
function beanstalk_uninstall() {
  drupal_uninstall_schema('beanstalk');
  variable_del('beanstalk_author');
  variable_del('beanstalk_links');
  variable_del('beanstalk_taxonomy');
  variable_del('beanstalk_vid');
  variable_del('beanstalk_og');
  variable_del('beanstalk_autosync');
  variable_del('beanstalk_account');
  variable_del('beanstalk_username');
  variable_del('beanstalk_password');
  variable_del('beanstalk_webhook_url');
}

/**
 * Implementation of hook_schema().
 */
function beanstalk_schema() {
  $schema['beanstalk_node_commit'] = array(
    'fields' => array(
      'vid'              => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'nid'              => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'revision'         => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'hash'             => array('type' => 'varchar', 'length' => 40),
      'time'             => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'changed_files'    => array('type' => 'text', 'default' => ''),
      'changed_dirs'     => array('type' => 'text', 'default' => ''),
      'changeset_url'    => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'author'           => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'author_full_name' => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'author_email'     => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'raw_data'         => array('type' => 'text', 'default' => ''),
    ),
    'primary key' => array('vid', 'nid'),
    'indexes' => array(
      'time' => array('time'),
    )
  );
  $schema['beanstalk_repository'] = array(
    'fields' => array(
      'rid'          => array('type' => 'serial', 'not null' => TRUE),
      'beanstalk_id' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'token'        => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'title'        => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'tids'         => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'gids'         => array('type' => 'varchar', 'length' => 255, 'default' => ''),
      'raw_data'     => array('type' => 'text', 'default' => ''),
    ),
    'primary key' => array('rid'),
    'unique keys' => array(
      'token' => array('token'),
      'beanstalk_id' => array('beanstalk_id'),
    ),
    'indexes'  => array(
      'title' => array('title'),
    ),
  );
  $schema['beanstalk_reference'] = array(
    'fields' => array(
      'commit_nid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
      'node_nid'   => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
    ),
    'primary key' => array('commit_nid', 'node_nid'),
    'indexes' => array(
      'node_nid' => array('node_nid'),
    )
  );
  return $schema;
}

/**
 * Add an extra field to repository and commit tables to store raw data.
 */
function beanstalk_update_6100() {
  $ret = array();
  db_add_field($ret, 'beanstalk_repository', 'raw_data', array('type' => 'text', 'default' => ''));
  db_add_field($ret, 'beanstalk_node_commit', 'raw_data', array('type' => 'text', 'default' => ''));
  return $ret;
}

/**
 * Add an extra field to commit table to stare git commit hashes.
 */
function beanstalk_update_6101() {
  $ret = array();
  db_add_field($ret, 'beanstalk_node_commit', 'hash', array('type' => 'varchar', 'length' => 40));
  return $ret;
}

/**
 * Add a field to repository table to stare beanstalk repository id.
 */
function beanstalk_update_6102() {
  $ret = array();
  db_add_field($ret, 'beanstalk_repository', 'beanstalk_id', array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0));
  return $ret;
}
