<?php

/**
 * Get a given resource.
 */
function beanstalk_resource_get($name, $options = array()) {
  return _beanstalk_resource($name, $options);
}

/**
 * Post to a given resource.
 */
function beanstalk_resource_post($name, $parameters = array(), $options = array()) {
  $json_parameters = str_replace('\/','/',json_encode($parameters));

  $post_options = array(
    'CURLOPT_POST' => TRUE,
    'CURLOPT_POSTFIELDS' => $json_parameters,
  );

  $options = array_merge($post_options, $options);

  return _beanstalk_resource($name, $options);
}

/**
 * Put to a given resource.
 */
function beanstalk_resource_put($name, $parameters = array(), $options = array()) {
  $json_parameters = str_replace('\/','/',json_encode($parameters));

  $post_options = array(
    // We can't use builtin cURL PUT because it expects a file as payload.
    'CURLOPT_CUSTOMREQUEST' => 'PUT',
    'CURLOPT_POSTFIELDS' => $json_parameters,
  );

  $options = array_merge($post_options, $options);

  return _beanstalk_resource($name, $options);
}

/**
 * Delete a given resource.
 */
function beanstalk_resource_delete($name, $options = array()) {
  $post_options = array(
    'CURLOPT_CUSTOMREQUEST' => 'POST',
    'CURLOPT_HTTPHEADER' => array('X-HTTP-Method-Override: DELETE')
  );

  $options = array_merge($post_options, $options);

  return _beanstalk_resource($name, $options);
}

/**
 * Access a given resource.
 */
function _beanstalk_resource($name, $options = array()) {
  list ($account, $username, $password) = beanstalk_autosync_credentials();
  $password_options = array('CURLOPT_USERPWD' => $username . ':' . $password);

  $options = array_merge($password_options, $options);

  $header_options = array(
    'CURLOPT_HTTPHEADER' => array('Accept: application/json', 'Content-Type: application/json', 'User-Agent: Papia Beanstalk Integration'),
    'CURLOPT_FRESH_CONNECT' => TRUE,
    'CURLOPT_RETURNTRANSFER' => TRUE,
    'CURLOPT_SSL_VERIFYPEER' => FALSE,
  );

  $options = array_merge_recursive($header_options, $options);

  $return = beanstalk_curl("https://$account.beanstalkapp.com/api/$name.json", $options);

  $json = json_decode($return);

  if (isset($json->errors)) {
    watchdog('beanstalk', 'Beanstalk error while trying to access the API resource %resource. Error response: <pre>!message</pre>', array('%resource' => $name, '!message' => $return), NULL, WATCHDOG_ERROR);
  }

  return $json;
}

/**
 * Do a cURL request.
 */
function beanstalk_curl($url, $options) {
  $curl = curl_init($url);

  foreach ($options as $key => $option) {
    if (substr($key, 0, 7) == 'CURLOPT') {
      curl_setopt($curl, constant($key), $option);
    }
  }

  return curl_exec($curl);
}
