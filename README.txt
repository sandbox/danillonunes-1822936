ABOUT
The beanstalk module provides integration with beanstalk webhooks. It was
developed by Daniel Dembach (http://dembachco.de/)


INSTALLATION
1. Extract tarball to sites/all/modules
2. Copy sites/all/modules/beanstalk/beanstalk.php to website root
3. Activate module at admin/build/modules
4. Configure at admin/settings/beanstalk
5. Add at least one repository at admin/settings/beanstalk/repositories/add
6. Configure repository url from admin/settings/beanstalk/repositories as
   webhook target in your beanstalk account (Repository->Setup->Integration)


CONFIGURATION
If you want to use beanstalk with organic groups and/or taxonomy, you have to
configure these, i.e. set up beanstalk_commit as a group post and add at least
one vocabulary.